<?php
class UI_Gravatar extends Plugin {

	/** @var string */
	private $gravatar_hash = "";

	function about() {
		return array(1.0,
			"Shows your globally recognized avatar (Gravatar) in the UI",
			"fox",
			false,
			"https://dev.tt-rss.org/fox/ttrss-ui-gravatar");
	}

	function init($host) {
		$sth = $this->pdo->prepare("SELECT email FROM ttrss_users WHERE id = ?");
		$sth->execute([($_SESSION['uid'] ?? 0)]);

		if ($row = $sth->fetch()) {
			$this->gravatar_hash = md5(trim($row['email']));
		}
	}

	function get_js() {
		if ($this->gravatar_hash) {
			return str_replace("%GRAVATAR_HASH%", $this->gravatar_hash,
				file_get_contents(__DIR__ . "/init.js"));
		} else {
			return "";
		}
	}

	function get_css() {
		if ($this->gravatar_hash) {
			return file_get_contents(__DIR__ . "/init.css");
		} else {
			return "";
		}
	}

	function api_version() {
		return 2;
	}

}
